import axios from 'axios'
const baseUrl = 'https://intro-react-ec5cf-default-rtdb.europe-west1.firebasedatabase.app/blogs.json'

const getAll = () => {
  const response = axios.get(baseUrl)
  return response.then(reponse => reponse.data)
}

const create = newObject => {
  const response = axios.post(baseUrl, newObject)
  return response.then(reponse => reponse.data)
}

export default {
  getAll, create
}