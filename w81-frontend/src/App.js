import React from 'react';
import { Routes, Route, Link, useMatch } from 'react-router-dom'
import { useState, useEffect } from 'react';
import blogService from './services/blog'
import About from './components/About/About'
import BlogForm from './components/BlogForm/BlogForm'
import Blog from './components/Blog/Blog'
import BlogList from './components/BlogList/BlogList'
import Footer from './components/Footer/Footer'
import { Navbar, Nav, Alert } from 'react-bootstrap'

function App() {
  const [blogs, setBlogs ] = useState([])
  const [notification, setNotification] = useState(null)

  useEffect(() => {
    blogService.getAll()
      .then(blogs => {
        const blogArray = Array.from(Object.keys(blogs), key => blogs[key])
        setBlogs( blogArray )
      })
      .catch(error => console.log(error))
  },[])

  const addBlog = (blog) => {
    blogService.create(blog)
      .then(newBlog => {
        setNotification(`a new blog ${blog.title} by ${blog.author} added`)
        setTimeout(() => {
          setNotification(null)
        }, 5000)
      })
      .catch((error) => {
        console.log(error)
        setNotification('Something went wrong')
        setTimeout(() => {
          setNotification(null)
        }, 5000)
      })
  }

  const match = useMatch('/blogs/:id')
  const blog = match
    ? blogs.find((b) => b.id === match.params.id)
    : null

  const text = {
    textDecoration: 'none',
    color: 'black'
  }

  const brand = {
    textDecoration: 'none',
    color: 'black',
    fontSize: '35px'
  }
  
  return (
    <div className="container">
        <Navbar collapseOnSelect bg="light" variant="light">
          <Navbar.Toggle aria-controls="responsive-navbar-nav" />
          <Navbar.Brand  >
            <Link style={brand} to="/">Blogs</Link>
          </Navbar.Brand>
          <Navbar.Collapse id="responsive-navbar-nav">
            <Nav className="mr-auto">
              <Nav.Link href="#" as="span">
                <Link style={text} to="/blogs">Blog List</Link>
              </Nav.Link>
              <Nav.Link href="#" as="span">
                <Link style={text} to="/create">Add new blog</Link>
              </Nav.Link>
              <Nav.Link href="#" as="span">
                <Link style={text} to="/about">About</Link>
              </Nav.Link>
            </Nav>
          </Navbar.Collapse>
        </Navbar>
      <div>
        {notification && <Alert variant="success">{notification}</Alert>}
      </div>
      <Routes>
        <Route path='/blogs/:id' element={<Blog blog={blog}/>}/>
        <Route path='/about' element={<About/>}/>
        <Route path='/create' element={<BlogForm addBlog={addBlog} />} />
        <Route path='/blogs' element={<BlogList blogs={blogs}/>}/>
        <Route exact path='/' element={<BlogList blogs={blogs}/>}/>
      </Routes>

      <Footer />
    </div>
  );
}

export default App;
