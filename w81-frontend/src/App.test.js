import { render, screen } from '@testing-library/react';
import { BrowserRouter } from 'react-router-dom';
import App from './App';

test('check blogs with navbar', () => {
  render(
    <BrowserRouter>
      <App />
    </BrowserRouter>
  );
  const navbar = screen.getByText('About');
  const navbar2 = screen.getByText('Blog List');
  const navbar3 = screen.getByText('Add new blog');
  expect(navbar).toBeInTheDocument();
  expect(navbar2).toBeInTheDocument();
  expect(navbar3).toBeInTheDocument();
});
