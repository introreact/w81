import React from "react";
import { render, screen } from '@testing-library/react'
import userEvent from '@testing-library/user-event'
import '@testing-library/jest-dom/extend-expect'
import BlogForm from './BlogForm'
import { BrowserRouter } from "react-router-dom";

test('<BlogForm /> updates parent state and calls onSubmit', () => {
  const addBlog = jest.fn()

  const component = render(
    <BrowserRouter>
      <BlogForm addBlog={addBlog} />
    </BrowserRouter>
  )

  const inputTitle = screen.getByPlaceholderText('title')
  const inputAuthor = screen.getByPlaceholderText('author')
  const inputUrl = screen.getByPlaceholderText('url')
  const button = screen.getByText('Submit')

  userEvent.type(inputTitle, 'testing of forms could be easier' )
  userEvent.type(inputAuthor, 'test author' )
  userEvent.type(inputUrl, 'test url' )
  userEvent.click(button)

  expect(addBlog.mock.calls).toHaveLength(1)
  expect(addBlog.mock.calls[0][0].title).toBe('testing of forms could be easier' )
  expect(addBlog.mock.calls[0][0].author).toBe('test author')
  expect(addBlog.mock.calls[0][0].url).toBe('test url')
})