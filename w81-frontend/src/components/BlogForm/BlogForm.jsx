import React from "react";
import { useState } from "react";
import { v4 as uuidv4 } from 'uuid'
import { Form, Button, FloatingLabel } from 'react-bootstrap'

const BlogForm = ({addBlog}) => {
  const [title, setTitle] = useState('')
  const [author, setAuthor] = useState('')
  const [url, setUrl] = useState('')

  const handleSubmit = (event) => {
    event.preventDefault()
    addBlog({
      title: title,
      author: author,
      url: url,
      id: uuidv4(),
      likes: 0
    })
    setTitle('')
    setAuthor('')
    setUrl('')
  }

  return (
    <div>
      <h2>Add a new blog</h2>
      <Form onSubmit={handleSubmit}>
          <FloatingLabel
            controlId="title"
            label="Title"
            className="mb-3"
          >
            <Form.Control 
            type="text" 
            value={title}
            onChange={({ target }) => setTitle(target.value)}
            placeholder="title" 
            />
          </FloatingLabel>

          <FloatingLabel
            controlId="author"
            label="Author"
            className="mb-3"
          >
            <Form.Control 
            type="text" 
            placeholder="author" 
            value={author}
            onChange={({ target }) => setAuthor(target.value)}
            />
          </FloatingLabel>

          <FloatingLabel
            controlId="url"
            label="Url"
            className="mb-3"
          >
            <Form.Control 
            type="text" 
            placeholder="url" 
            value={url}
            onChange={({ target }) => setUrl(target.value)}
            />
          </FloatingLabel>
          <Button variant="secondary" type="submit">
            Submit
          </Button>
      </Form>
    </div>
  )
}

export default BlogForm