import React from "react";
import '@testing-library/jest-dom/extend-expect'
import { render } from '@testing-library/react'
import Blog from './Blog'
import { BrowserRouter } from "react-router-dom";

test('render author, title, and like by default', () => {
  const blog = {
    title: 'test title',
    author: 'Hung Nguyen',
    url: 'test url',
    likes: 0
  }

  const component = render(
    <BrowserRouter>
      <Blog blog={blog} />
    </BrowserRouter>
  )


  expect(component.container).toHaveTextContent(blog.author)
  expect(component.container).toHaveTextContent(blog.title)
  expect(component.container).toHaveTextContent(blog.likes)
})