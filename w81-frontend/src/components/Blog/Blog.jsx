import React from "react";

const Blog = ({blog, removeBlog}) => {

  return (
    <div>
      <h3>{blog.title}</h3>
      <p>Author: {blog.author}</p>
      <p>Likes: {blog.likes}</p>
      <p><a href={blog.url} target="_blank">See in details</a></p>
      
    </div>
  )
}

export default Blog