import React from "react";
import "./Footer.css"

const Footer = () => {
  return (
    <div className="footer">
      <p>Final Project of 'Introduction to Programming' course</p>
      <a href="https://www.linkedin.com/in/hung-nguyen-b9a1891a7/">@HungNguyen</a>
    </div>
  )
}

export default Footer