import React from "react"
import { Link } from "react-router-dom"
import { Table } from "react-bootstrap"

const BlogList = ({blogs}) => {
  const text = {
    textDecoration: 'none',
    color: 'black',
    fontStyle: 'bold'

  }
  return (
    <div>
      <h2>List of blogs</h2>
      <Table>
        <thead>
          <tr>
            <th>Title</th>
          </tr>
        </thead>
        <tbody>
          {blogs.map(blog =>
            <tr key={blog.id}>
              <td>
                <Link style={text} to={`/blogs/${blog.id}`}>{blog.title}</Link>
              </td>
            </tr>
          )}
        </tbody>
      </Table>
    </div>
  )
}

export default BlogList