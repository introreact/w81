import React from "react";
import '@testing-library/jest-dom/extend-expect'
import { render } from '@testing-library/react'
import BlogList from './BlogList'
import { BrowserRouter } from "react-router-dom";

test('Title of blogs are links', () => {
  const blogs = [
    { 
      id: 1,
      title: 'test title',
      author: 'Hung Nguyen',
      url: 'test url',
      likes: 0
    },
    {
      id: 2,
      title: 'test title 2',
      author: 'Hung Nguyen',
      url: 'test url',
      likes: 0
    }
  ]

  const component = render(
    <BrowserRouter>
      <BlogList blogs={blogs}/>
    </BrowserRouter>
  )

  expect(component.container).toHaveTextContent(blogs[0].title)
  expect(component.container).toHaveTextContent(blogs[1].title)
})